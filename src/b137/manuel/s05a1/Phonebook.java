package b137.manuel.s05a1;

import java.util.ArrayList;

public class Phonebook {

    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    public Phonebook() {}

    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }
    //getter
    public ArrayList<Contact> getPhonebook() {
        return contacts;
    }
    //setter
    public void setPhonebook(ArrayList<Contact> phoneBook) {
        this.contacts = phoneBook;
    }
    // add contact
    public void addContact(Contact contact) {
        this.contacts.add(contact);
    }

    // if is empty
    public boolean isEmpty() {
        return this.contacts.size() == 0;
    }




}
