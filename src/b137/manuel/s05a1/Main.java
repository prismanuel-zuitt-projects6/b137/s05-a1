package b137.manuel.s05a1;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        //Instantiate 2 contacts from the Contact class
        //Add both contacts to the phonebook object using its setter method
        //Define control structure that will output a notif message if empty or print all
        //
        Contact contact1 = new Contact();
        Contact contact2 = new Contact();

        contact1.setName("John Doe");
        contact1.addNumber("+639152468596");
        contact1.addNumber("+639228547963");
        contact1.addAddress("my home in Quezon City");
        contact1.addAddress("my office in Makati City");

        contact2.setName("Jane Doe");
        contact2.addNumber("+639162148573");
        contact2.addNumber("+639173698541");
        contact2.addAddress("my home in Caloocan City");
        contact2.addAddress("my office in Pasay City");

        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        if (phonebook.isEmpty()) {
            System.out.println("Phonebook is empty");
        } else {
            for (Contact contact : phonebook.getPhonebook()) {
                System.out.println(contact.getName());
                System.out.println("-------------------");
                System.out.println(contact.getName() + " has the following registered numbers:");
                contact.printNumbers();
                System.out.println("----------------------------------");
                System.out.println(contact.getName() + " has the following registered addresses:");
                contact.printAddresses();
                System.out.println("==================================");
            }
        }


    }
}
